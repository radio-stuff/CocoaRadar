//
//  MyDocument.m
//  CocoaRadar
//
//  Created by William Dillon on 10/29/08.
//  Copyright __MyCompanyName__ 2008 . All rights reserved.
//

#import "MyDocument.h"

@implementation MyDocument

- (id)init
{
    self = [super init];
    if (self) {
    
        // Add your subclass-specific initialization here.
        // If an error occurs here, send a [self release] message and return nil.
    
    }
    return self;
}

- (NSString *)windowNibName
{
    return @"MyDocument";
}

- (void)windowControllerDidLoadNib:(NSWindowController *) aController
{
    [super windowControllerDidLoadNib:aController];
	
	[radarOpenGLController setRadar:radar];
}

- (BOOL)readFromURL:(NSURL *)absoluteURL ofType:(NSString *)typeName error:(NSError **)outError
{
	char stationID[5];

	if( [absoluteURL isFileURL] ) {
		path = [absoluteURL path];
		NSLog(@"File name: %@", path);
		
		NSString *fileName = [[path componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"/\\"]] lastObject];
		const char *stationID_temp = [[fileName substringToIndex:4] cStringUsingEncoding:NSUTF8StringEncoding];
		strncpy(stationID, stationID_temp, sizeof(stationID));
		
		NSLog(@"Reading from WSR-88D site: %s", stationID);
		
		radar = RSL_wsr88d_to_radar((char *)[path cStringUsingEncoding:NSUTF8StringEncoding], stationID);
		if( radar == NULL ) {
			NSLog(@"File processing failed.");
			if ( outError != NULL ) {
				*outError = [NSError errorWithDomain:NSOSStatusErrorDomain code:unimpErr userInfo:NULL];
			}
			return NO;
		}
		
	} else {
		NSLog(@"URL given not a file.");
		if ( outError != NULL ) {
			*outError = [NSError errorWithDomain:NSOSStatusErrorDomain code:unimpErr userInfo:NULL];
		}
	
		return NO;
	}

	
	
    return YES;
}

@end
