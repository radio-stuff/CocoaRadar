//
//  RadarOpenGLController.m
//  CocoaRadar
//
//  Created by William Dillon on 8/11/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "RadarOpenGLController.h"
#import "Texture.h"

@implementation RadarOpenGLController

#pragma mark -
#pragma mark Init and bookkeeping methods
+ (NSOpenGLPixelFormat *)defaultPixelFormat
{
    NSOpenGLPixelFormatAttribute attributes [] = {
        NSOpenGLPFAWindow,
		NSOpenGLPFADoubleBuffer,
		NSOpenGLPFADepthSize, 16,
        (NSOpenGLPixelFormatAttribute)nil };
	
    return [[[NSOpenGLPixelFormat alloc] initWithAttributes:attributes] autorelease];
}

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	radar = nil;
	volumeTexture = nil;
	
}

- (void)initGL
{
	NSLog(@"In radar view initGL routine");

// Use the superclasses camera management
	[super initGL];
	radar = nil;
	
// Set black background
	glClearColor(0.0, 0.0, 0.0, 1.0);
	
// Set blending characteristics
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

- (void)draw
{
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	float depth = [depthSlider floatValue];
//	float depth = .5;
	
	[volumeTexture bind];
	
	glColor3f(1., 0., 0.);
	glBegin(GL_QUADS); {
		glTexCoord3f( 1.,  1., depth );
		glVertex2f(   1.,  1.);
		glTexCoord3f( 0.,  1., depth );
		glVertex2f(  -1.,  1.);
		glTexCoord3f( 0.,  0., depth );
		glVertex2f(  -1., -1.);
		glTexCoord3f( 1.,  0., depth );
		glVertex2f(   1., -1.);
		glTexCoord3f( 1.,  1., depth );
		glVertex2f(   1.,  1.);
	} glEnd();

	[volumeTexture unBind];	
}

- (IBAction)depthChanged:(id)sender
{
	[self updateData:sender];
}

- (void)setRadar:(Radar *)newRadar
{
	radar = newRadar;

/*********************************
 * Find the volumes for the file *
 *********************************/
	if( volumes != nil ) free(volumes);
	volumes = (int *)malloc(radar->h.nvolumes * sizeof(int));
	nvolumes = 0;
	
	for( int i = 0; i < radar->h.nvolumes; i++ ) {
		// This may be an empty volume pointer
		if( radar->v[i] != NULL ) {
			volumes[nvolumes] = i;
			nvolumes++;
			NSLog(@"Volume %d type: %s", nvolumes, radar->v[i]->h.type_str);
		}
	}

	if( nvolumes > 3 ) {
		NSLog(@"Read %d volumes; unable to deal with 3 volumes at this time.", nvolumes);		
	}
	
	realloc(volumes, nvolumes * sizeof(int));
	
/*********************************************
 * Find the sweeps for the volume			 *
 * (assume all volumes have the same sweeps) *
 *********************************************/
//	if( sweeps  != nil ) free(sweeps);
//	sweeps  = (int **)malloc(nvolumes * sizeof(int *));
//	nsweeps = 0;
//
//	// Find the highest size of each dimension for each level
//	for( int v = 0; v < nvolumes; v++ ) {
//		sweeps[v]  = (int *)malloc(radar->v[volumes[0]]->h.nsweeps * sizeof(int));
//		int thisSweeps = 0;
//	
//		// Count and track all non-zero sweeps for this volume
//		for( int i = 0; i < radar->v[volumes[0]]->h.nsweeps; i++ ) {		
//			// This may be an empty sweep pointer
//			if( radar->v[volumes[0]]->sweep[i] != NULL ) {
//				sweeps[v][nsweeps] = i;
//				nsweeps++;
//			}
//		}		
//		nsweeps = (nsweeps > thisSweeps)? nsweeps : thisSweeps;
//		NSLog(@"Read %d sweepsin volume %d", nsweeps, v);
//		realloc(sweeps[v], thisSweeps * sizeof(int));
//	}
		

/*******************************************
 * Find the rays contained in the volume   *
 * (assume all volumes have the same rays) *
 *******************************************/
//	for( int i = 0; i < nrays; i++ ) {
//		rays[i] = -1;
//	}
	
//	for( int i = 0; i < radar->v[volumes[0]]->sweep[sweeps[0]]->h.nrays; i++ ) {		
		// This may be an empty ray pointer
//		if( radar->v[volumes[0]]->sweep[sweeps[0]]->ray[i] != NULL ) {
//			nrays++;
		// Set the this ray's index according to it's azimuth
//			rays[((int)(radar->v[volumes[0]]->sweep[sweeps[0]]->ray[i]->h.azimuth + .5)) % 360] = i;			
			
//			NSLog(@"Ray %f stored at %d; original bin %d; time: %02d:%02d:%1.2f ",
//				  radar->v[volumes[0]]->sweep[sweeps[0]]->ray[i]->h.azimuth,
//				  ((int)(radar->v[volumes[0]]->sweep[sweeps[0]]->ray[i]->h.azimuth + .5)) % 360, i,
//				  radar->v[volumes[0]]->sweep[sweeps[0]]->ray[i]->h.hour,
//				  radar->v[volumes[0]]->sweep[sweeps[0]]->ray[i]->h.minute,
//				  radar->v[volumes[0]]->sweep[sweeps[0]]->ray[i]->h.sec);
//		}
//	}

//	for( int i = 0; i < nrays; i++ ) {
//		if( rays[i] == -1 ) NSLog(@"Azmuth %d not set.", i);
//	}
	
//	NSLog(@"Read %d rays", nrays);

/**************************************************
 * Using the volume dimensions create 3D textures *
 * (assume all volumes have the same dimensions)  *
 **************************************************/

	nsweeps = 0;
	for( int v = 0; v < nvolumes; v++ ) {
		int thisSweeps = radar->v[volumes[0]]->h.nsweeps;
		nsweeps = (nsweeps > thisSweeps)? nsweeps : thisSweeps;
	}
		
	nrays = 0;
	for( int v = 0; v < nvolumes; v++ ) {
		for( int s = 0; s < radar->v[volumes[0]]->h.nsweeps; s++ ) {
			int thisRays = radar->v[volumes[v]]->sweep[s]->h.nrays;
			nrays = (nrays > thisRays)? nrays : thisRays;
		}
	}
	
	nbins = 0;
	for( int v = 0; v < nvolumes; v++ ) {
		for( int s = 0; s < radar->v[volumes[0]]->h.nsweeps; s++ ) {
			for( int r = 0; r < radar->v[volumes[v]]->sweep[s]->h.nrays; r++ ) {
				int thisBins = radar->v[volumes[v]]->sweep[s]->ray[r]->h.nbins;
				nbins = (nbins > thisBins)? nbins : thisBins;				
			}
		}
	}

	volumeTexture = [[Texture alloc] initWithWidth:nbins height:NUM_RAYS depth:nsweeps channels:4 type:GL_FLOAT];
	
/***********************************************
 * Copy the radar structure data into textures *
 ***********************************************/
	int nearest[2];
	float nearestScalar[2];
	
	float *scalars = (float *)malloc(sizeof(float) * nsweeps * NUM_RAYS);
	float *bytes = (float *)[volumeTexture textureBytes];

	// Clear the volume
	for( int s = 0; s < nsweeps; s++ ) {
		for( int r = 0; r < NUM_RAYS; r++ ) {
			scalars[(s*NUM_RAYS) + r] = 0.;
			for( int b = 0; b < nbins; b++ ) {
				for( int v = 0; v < nvolumes; v++ ) {
					bytes[(s*NUM_RAYS*nbins*4) + (r*nbins*4) + (b*4) + v] = 0.;
					bytes[(s*NUM_RAYS*nbins*4) + (r*nbins*4) + (b*4) + 3] = 0.;
				}
			}
		}
	}

	// For each radar sample apply it proportionatly to it's nearest integer bins
//	for( int v = 0; v < nvolumes; v++ ) {
//TODO: Fix the problms with the other volumes (doppler velocity and spectrum width)
	int v = 0; {
		for( int s = 0; s < radar->v[volumes[v]]->h.nsweeps; s++ ) {
			if( radar->v[volumes[v]]->sweep[s] == NULL ) continue;
			for( int r = 0; r < radar->v[volumes[v]]->sweep[s]->h.nrays; r++ ) {
				if( radar->v[volumes[v]]->sweep[s]->ray[r] == NULL ) continue;
				nearest[0] =  (int)floor(radar->v[volumes[v]]->sweep[s]->ray[r]->h.azimuth);
				nearest[1] =   (int)ceil(radar->v[volumes[v]]->sweep[s]->ray[r]->h.azimuth) % 360; // Wrap around, 360 = 0
				nearestScalar[0] = fmod(1. - (radar->v[volumes[v]]->sweep[s]->ray[r]->h.azimuth - nearest[0]), 360.);
				nearestScalar[1] = fmod(1. - (nearest[1] - radar->v[volumes[v]]->sweep[s]->ray[r]->h.azimuth), 360.);
				// For the first volume (it doesn't really matter, but it makes no sense to do it 3 times) calculate scaling coefficients
				if( v == 0 ) {
					scalars[(s*NUM_RAYS) + nearest[0]] += nearestScalar[0];
					scalars[(s*NUM_RAYS) + nearest[1]] += nearestScalar[1];					
				}
				// For each range bin, copy the radar data into the texture ram
				for( int b = 0; b < radar->v[volumes[v]]->sweep[s]->ray[r]->h.nbins; b++ ) {
					float rangeVal = radar->v[volumes[v]]->sweep[s]->ray[r]->h.f(radar->v[volumes[v]]->sweep[s]->ray[r]->range[b]);
					// Check whether the radar library thinks this is a valid sample, if so copy and set opaque
					if( rangeVal != BADVAL ) {
						rangeVal /= 40.;
						bytes[(s*NUM_RAYS*nbins*4) + (nearest[0]*nbins*4) + (b*4) + v] += nearestScalar[0] * rangeVal;
						bytes[(s*NUM_RAYS*nbins*4) + (nearest[1]*nbins*4) + (b*4) + v] += nearestScalar[1] * rangeVal;
						bytes[(s*NUM_RAYS*nbins*4) + (nearest[1]*nbins*4) + (b*4) + 3]  = 1.;
					// Otherwise, don't copy and set transparent
					} else {
						bytes[(s*NUM_RAYS*nbins*4) + (nearest[1]*nbins*4) + (b*4) + 3]  = 0.;
					}
				}
			}
		}
	}	
	
	// Re-scale all values based upon the contributions
	for( int v = 0; v < nvolumes; v++ ) {
		for( int s = 0; s < radar->v[volumes[v]]->h.nsweeps; s++ ) {
			for( int r = 0; r < NUM_RAYS; r++ ) {
				for( int b = 0; b < radar->v[volumes[v]]->sweep[s]->ray[r]->h.nbins; b++ ) {
					bytes[(s*NUM_RAYS*nbins*4) + (r*nbins*4) + (b*4) + v] *= 1./scalars[(s*NUM_RAYS) + r];
				}
			}
		}
	}
	
	// Load the new values into the graphics driver
	[volumeTexture updateData];

	NSLog(@"Finished copying radar data into textures");
	
}

@end
