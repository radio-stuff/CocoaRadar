//
//  RadarOpenGLController.h
//  CocoaRadar
//
//  Created by William Dillon on 8/11/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "OpenGLController.h"
#import <RSL/rsl.h>

#define NUM_RAYS 360

@class Texture;

@interface RadarOpenGLController : OpenGLController {
	Texture *mapTexture;

	IBOutlet NSSlider *depthSlider;
	
	Radar *radar;
	int *volumes,	nvolumes;
	int **sweeps,	nsweeps;
	int  rays[NUM_RAYS];
	int  nrays; 	// Force integer spaced rays (365)
	int  nbins;

	Texture *volumeTexture;
}

- (void)setRadar:(Radar *)newRadar;
- (IBAction)depthChanged:(id)sender;

@end
