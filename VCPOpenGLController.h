//
//  VCPOpenGLController.h
//  CocoaRadar
//
//  Created by William Dillon on 10/30/08.
//  Copyright 2008 Oregon State University. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "OpenGLController.h"
#import <RSL/rsl.h>

@class Texture;

@interface VCPOpenGLController : OpenGLController {
	Radar *radar;
	GLuint texIDs[1];

}

- (void)setRadar:(Radar *)newRadar;

@end
