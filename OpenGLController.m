//
//  OpenGLController.m
//
//  Created by William Dillon on 11/24/06.
//  Copyright 2006 VIA Computing. All rights reserved.
//  Permission Granted to Oregon State University for any use.
//

#import "OpenGLController.h"

#define VIEW_LINE 0
#define VIEW_2D   1
#define VIEW_BOTH 2

@implementation OpenGLController

- (void)awakeFromNib
{
//	NSLog(@"OpenGLController awoke from NIB file.");

	timer = nil;
}

+ (NSOpenGLPixelFormat *)defaultPixelFormat
{
    NSOpenGLPixelFormatAttribute attributes [] = {
        NSOpenGLPFAWindow,
		NSOpenGLPFADoubleBuffer,
		NSOpenGLPFADepthSize, 16,
	(NSOpenGLPixelFormatAttribute)nil };
	
    return [[[NSOpenGLPixelFormat alloc] initWithAttributes:attributes] autorelease];
}

- (void)initGL
{
	NSLog(@"Base class init GL");
	
	glClearColor( 0.0, 0.0, 0.0, 1.0 );
		
	glMatrixMode(GL_PROJECTION);
	glOrtho(-1.5, 1.5, -1.5, 1.5, -1.5, 1.5);
	glMatrixMode(GL_MODELVIEW);		

	return;
}

float changeOrder(float input) {
	char *inBytes = (char *)&input;
	char outBytes[4];
	
	outBytes[3] = inBytes[0];
	outBytes[2] = inBytes[1];
	outBytes[1] = inBytes[2];
	outBytes[0] = inBytes[3];

	float *value = (float *)outBytes;
	
	return *value;
}

- (void)draw
{	
	
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	
	return;
}

- (void)reshape:(NSRect)rect
{
	glViewport(0, 0, rect.size.width, rect.size.height);
	
	aspectRatio = (float)rect.size.width / (float)rect.size.height;
	
//	NSLog(@"Screen Resized: (%f, %f) new aspect ratio: %f.\n", (float)rect.size.width, (float)rect.size.height, aspectRatio);
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if( aspectRatio > 1. ) {	// Wide
		glOrtho(-1. * aspectRatio,
				 1. * aspectRatio,
				-1., 1.,
				-1.5, 1.5);

	} else {					// Tall
		glOrtho(-1., 1.,
				-1. * aspectRatio,
				 1. * aspectRatio,
				-1.5, 1.5);		
	}
	glMatrixMode(GL_MODELVIEW);	
	
	return;
}

#pragma mark -
#pragma mark Setter Methods
- (void)setView:(id)sender
{
	openGLView = sender;
}

- (void)setAspectRatio:(GLfloat)aRatio
{
	aspectRatio = aRatio;
}

- (void)requestRedraw:(NSTimer*)theTimer
{
	[openGLView setNeedsDisplay: YES];
}

#pragma mark -
#pragma mark User Interface logic
- (void)mouseDownLocation:(NSPoint)location Flags:(unsigned int)modifierFlags;
{
	oldMouse = location;

	return;
}

- (void)mouseDraggedLocation:(NSPoint)location Flags:(unsigned int)modifierFlags
{	
	[openGLView setNeedsDisplay: YES];
	
	return;
}

- (void)scrollWheel:(NSEvent *)theEvent
{
	scale += scale * [theEvent deltaY] / 100.;
	[openGLView setNeedsDisplay: YES];
}

- (IBAction)updateData:(id)sender
{
	[openGLView setNeedsDisplay: YES];
}

@end
