//
//  Quaternion.m
//  BeamFormerUI
//
//  Created by William Dillon on 7/17/07.
//  Copyright 2007 VIA Computing. All rights reserved.
//

#import "Quaternion.h"
#import <OpenGL/gl.h>

#define CENTER_X 0
#define CENTER_Y 1
#define WIDTH	 2
#define HEIGHT   3

@implementation Quaternion

// Prototypes for internal functions
double  unit ( double  vin[3], double  vout[3] );
double	dot  ( double left[3], double right[3] );
void	cross( double left[3], double right[3], double result[3] );
void	matrixMult ( double left[16], double right[16] );
void	quat2matrix( double quat[4],  double matrix[16] );

- (id)init
{
	self = [super init];
	
	if( self != nil ) {
	// Initialize the Rotation matrix to identity
		int i;
		for( i = 0; i < 16; i++ ) {
			rotationMatrix[i] = 0.;
		}
		rotationMatrix[ 0] = 1.;
		rotationMatrix[ 5] = 1.;
		rotationMatrix[10] = 1.;
		rotationMatrix[15] = 1.;		
	}
	
	return self;
}

- (void)applyRotation
{
	glMultMatrixd( (GLdouble*)rotationMatrix );
}

// Update the bounds of the viewport
- (void)screenResize
{
	glGetIntegerv( GL_VIEWPORT, (GLint*)viewport );
//	NSLog(@"Loaded new viewport dimensions: %d, %d, %d, %d.\n", viewport[0], viewport[1], viewport[2], viewport[3] );
}


// Begin tracking the mouse movement with the trackBall
- (void)mouseDown:(NSPoint)location
{
	[self map2sphere:location vector:startVector];
}


// Track mouse movement
- (void)mouseMotion:(NSPoint)location
{
	double	axisOfRotation[3];
	double	dragQuaternion[4];
	double	tempRotMatrix[16];
	
	// Get the vector of the second start point
	[self map2sphere:location vector:endVector];
	
	// Create a quaternion of the drag
	cross( startVector, endVector, axisOfRotation );

	int i;
	for( i = 0; i < 3; i++ )
		dragQuaternion[i] = axisOfRotation[i];

	dragQuaternion[3] = dot( startVector, endVector );	
	
	// Make a rotation matrix from the drag quaternion
	quat2matrix( dragQuaternion, tempRotMatrix );

	// Multiply the temp matrix with the rotation matrix
	matrixMult( (double*)rotationMatrix, tempRotMatrix );

	// Use the most recent vector as the start vector and discard the old
	startVector[0] = endVector[0];
	startVector[1] = endVector[1];
	startVector[2] = endVector[2];
}

/******** INTERNAL USE FUNCTIONS **********/
- (void)map2sphere:(NSPoint)location vector:(double[3])theVector
{
	double temp[3];
	GLfloat magnitude;
	
	// Map mouse coords to the universal device coords
	float x = (location.x / ( (viewport[WIDTH]  - 1.0) / 2.0 )) - 1;
	float y = (location.y / ( (viewport[HEIGHT] - 1.0) / 2.0 )) - 1;
	
	// Load a vector with the device coords and 0 z component
	temp[0] = x;
	temp[1] = y;
	temp[2] = 0.0;
	
	// Find the magnitude of the vector by taking the dot product
    magnitude = dot(temp, temp);
	
    //If the point is mapped outside of the sphere... (length > radius squared)
    if (magnitude > 1.0f) {
		unit( temp, temp);
		
        //Return the "normalized" vector, a point on the sphere
        theVector[0] = temp[0];
        theVector[1] = temp[1];
        theVector[2] = temp[2];
    } else {
        //Return a vector to a point mapped inside the sphere sqrt(radius squared - length)
        theVector[0] = temp[0];
        theVector[1] = temp[1];
        theVector[2] = sqrt( 1.0f - magnitude );
    }
}

@end

/****** MATH FUNCTIONS ******/
double
dot( double left[3], double right[3] )
{
	double temp[3];
	
	temp[0] = left[0] * right[0];
	temp[1] = left[1] * right[1];
	temp[2] = left[2] * right[2];

	return temp[0] + temp[1] + temp[2];
}

void
cross( double left[3], double right[3], double result[3] )
{
	result[0] =  left[1] * right[2] - right[1] *  left[2];
	result[1] = right[0] *  left[2] -  left[0] * right[2];
	result[2] =  left[0] * right[1] - right[0] *  left[1];
}

void
matrixMult( double left[16], double right[16] ) {
	float accum[16];

#define ROWCOL(i, j) \
	left[i*4 + 0] * right[ 0 + j] + \
	left[i*4 + 1] * right[ 4 + j] + \
	left[i*4 + 2] * right[ 8 + j] + \
	left[i*4 + 3] * right[12 + j]
	accum[0 ] = ROWCOL(0,0); accum[1 ] = ROWCOL(0,1); accum[2 ] = ROWCOL(0,2); accum[3 ] = ROWCOL(0,3); 
	accum[4 ] = ROWCOL(1,0); accum[5 ] = ROWCOL(1,1); accum[6 ] = ROWCOL(1,2); accum[7 ] = ROWCOL(1,3); 
	accum[8 ] = ROWCOL(2,0); accum[9 ] = ROWCOL(2,1); accum[10] = ROWCOL(2,2); accum[11] = ROWCOL(2,3); 
	accum[12] = ROWCOL(3,0); accum[13] = ROWCOL(3,1); accum[14] = ROWCOL(3,2); accum[15] = ROWCOL(3,3); 
#undef ROWCOL

	int i;
	for( i = 0; i < 16; i++ ) {
		left[i] = accum[i];
	}
}

void
quat2matrix( double quat[4], double matrix[16] ) {
	float xs, ys, zs, wx, wy, wz, xx, xy, xz, yy, yz, zz;
	
    float t  = 2.0f / ( dot(quat, quat) + quat[3] * quat[3] );
	
    xs = quat[0]*t;		ys = quat[1]*t;		zs = quat[2]*t;
    wx = quat[3]*xs;	wy = quat[3]*ys;	wz = quat[3]*zs;
    xx = quat[0]*xs;	xy = quat[0]*ys;	xz = quat[0]*zs;
    yy = quat[1]*ys;	yz = quat[1]*zs;	zz = quat[2]*zs;
	
    matrix[0 ] = 1.0f-(yy+zz);	matrix[1 ] = xy+wz;			matrix[2 ] = xz-wy;			matrix[3 ] = 0.0f;
	matrix[4 ] = xy-wz;			matrix[5 ] = 1.0f-(xx+zz);	matrix[6 ] = yz+wx;			matrix[7 ] = 0.0f;
	matrix[8 ] = xz+wy;			matrix[9 ] = yz-wx;			matrix[10] = 1.0f-(xx+yy);	matrix[11] = 0.0f;
	matrix[12] = 0.0f;			matrix[13] = 0.0f;			matrix[14] = 0.0f;			matrix[15] = 1.0f;	
}

double
unit( double vin[3], double vout[3] )
{
	double dist, f;
	
	dist = vin[0]*vin[0] + vin[1]*vin[1] + vin[2]*vin[2];
	
	if( dist > 0.0 )
	{
		dist = sqrt( dist );
		f = 1. / dist;
		vout[0] = f * vin[0];
		vout[1] = f * vin[1];
		vout[2] = f * vin[2];
	}
	else
	{
		vout[0] = vin[0];
		vout[1] = vin[1];
		vout[2] = vin[2];
	}
	
	return( dist );
}
