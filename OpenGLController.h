//
//  OpenGLController.h
//  Forest Map NCD
//
//  Created by William Dillon on 11/24/06.
//  Copyright 2006 VIA Computing. All rights reserved.
//  Permission granted to Oregon State University for all use.
//

#import <Cocoa/Cocoa.h>
#import "OpenGLController.h"
#import "OpenGL/gl.h"
#import "ShaderProgram.h"

@class OpenGLView;
@class MyDocument;
@class NetworkSession;

#define MOTION_BLUR

#define CHANNELS 8
#define CORRELATIONS 4
#define FRAMES 1024

@interface OpenGLController : NSObject {
	IBOutlet OpenGLView *openGLView;
	IBOutlet NSDocument *myDocument;
		
	// Projection tramsform values
	GLdouble scale;
	GLfloat  aspectRatio;
	
	// Old mouse location for dragging
	NSPoint oldMouse;
		
	// Screen refresh timer
	NSTimer *timer;
}

// Get initialization parameters
+ (NSOpenGLPixelFormat *)defaultPixelFormat;

// UI IBActions and Events
- (IBAction)updateData:(id)sender;

- (void)mouseDownLocation:(NSPoint)location Flags:(unsigned int)modifierFlags;
- (void)mouseDraggedLocation:(NSPoint)location Flags:(unsigned int)modifierFlags;
- (void)scrollWheel:(NSEvent *)theEvent;

// Setters
- (void)setView:(id)view;
- (void)setAspectRatio:(GLfloat)aRatio;

// OpenGL Methods and callbacks
- (void)initGL;
- (void)reshape:(NSRect)rect;
- (void)draw;
- (void)requestRedraw:(NSTimer*)theTimer;

@end
