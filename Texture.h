//
//  Texture.h
//  Cocoa OpenGL
//
//  Created by William Dillon on 7/18/07.
//  Copyright 2007. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "OpenGL/gl.h"

@interface Texture : NSObject {
	int				 width;
	int				 height;
	int				 depth;
	unsigned int	 textureID;
	void			*textureBytes;

	GLint			 textureType;
	GLint			 type;
	GLint			 format;
	
	GLint			 minFilter;
	GLint			 magFilter;
	GLint			 wrapS;
	GLint			 wrapT;
	GLint			 wrapR;
	GLint			 environment;
	
	bool			 isLoaded;
}

// Initialization functions (2D)
//- (id)initWithFile:(NSString *)fileName;

- (id)initWithBytes:(void *)bytes
		   width:(int)width
		  height:(int)height;

// Initialization functions (3D)
- (id)initWithBytes:(void *)bytes
		   width:(int)width
		  height:(int)height
		   depth:(int)depth;

// Initialization w/o previous space
- (id)initWithWidth:(int)width
			 height:(int)height
			  depth:(int)depth;

- (id)initWithWidth:(int)thewidth
			 height:(int)theheight
			  depth:(int)thedepth
		   channels:(int)chans
			   type:(GLint)newType;

// Texture parameters
@property(readwrite)	GLint			 magFilter;		// Default: GL_LINEAR
@property(readwrite)	GLint			 minFilter;		// Default: GL_LINEAR
@property(readwrite)	GLint			 wrapS;			// Default: GL_CLAMP
@property(readwrite)	GLint			 wrapT;			// Default: GL_CLAMP
@property(readwrite)	GLint			 wrapR;			// Default: GL_CLAMP
@property(readwrite)	GLint			 environment;	// Default: GL_REPLACE

@property(readonly)		unsigned int	 textureID;
@property(readonly)		int				 width;
@property(readonly)		int				 height;
@property(readonly)		void			*textureBytes;
@property(readonly)		GLint			 format;
@property(readonly)		GLint			 type;
@property(readonly)		GLint			 textureType;

// Texture actions
- (void)bind;
- (void)updateData;
- (void)unBind;

@end
