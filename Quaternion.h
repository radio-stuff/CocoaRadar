//
//  Quaternion.h
//  BeamFormerUI
//
//  Created by William Dillon on 7/17/07.
//  Copyright 2007 VIA Computing. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <OpenGL/gl.h>

@interface Quaternion : NSObject {
	
	// Our current rotation matrix
	GLdouble rotationMatrix[16];
	
	// State variables
	int viewport[4];
	NSPoint mouse;
	
	double startVector[3];
	double   endVector[3];
}

// Initialization
- (id)init;

// User callbacks
- (void)mouseDown:(NSPoint)location;
- (void)mouseMotion:(NSPoint)location;
- (void)screenResize;

- (void)applyRotation;

// Internal Functions (users shouldn't call these)
- (void)map2sphere:(NSPoint)location vector:(double[3])theVector;


@end
