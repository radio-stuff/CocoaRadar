//
//  ShaderProgram.h
//  HD Audio UI
//
//  Created by William Dillon on 6/19/08.
//  Copyright 2008 Oregon State University. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "OpenGL/gl.h"

@interface ShaderProgram : NSObject {
	GLuint theProgram, vertexShaderID, fragShaderID;
}

- (id)initWithVertex:(NSString *)vertexSource andFragment:(NSString *)fragmentSource;

- (GLint)getUniformLocationForString:(NSString *)uniform;
- (void)setUniformValueForString:(NSString *)uniform withInt:(GLint)value;
- (void)setUniformValueForString:(NSString *)uniform withFloat:(GLfloat)value;
- (void)setUniformValueForString:(NSString *)uniform withDouble:(GLdouble)value;

- (void)bind;
- (void)unBind;

@end
