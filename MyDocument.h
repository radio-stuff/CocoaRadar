//
//  MyDocument.h
//  CocoaRadar
//
//  Created by William Dillon on 10/29/08.
//  Copyright __MyCompanyName__ 2008 . All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "RSL/rsl.h"

#import "RadarOpenGLController.h"
#import "VCPOpenGLController.h"

@interface MyDocument : NSDocument
{
	IBOutlet RadarOpenGLController	*radarOpenGLController;
	IBOutlet VCPOpenGLController	*vcpOpenGLController;
	
	Radar	 *radar;
	NSString *path;
	float	 *elevations;
	
}

- (BOOL)readFromURL:(NSURL *)absoluteURL ofType:(NSString *)typeName error:(NSError **)outError;

@end
