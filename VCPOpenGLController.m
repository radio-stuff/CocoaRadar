//
//  VCPOpenGLController.m
//  CocoaRadar
//
//  Created by William Dillon on 10/30/08.
//  Copyright 2008 Oregon State University. All rights reserved.
//

#import "VCPOpenGLController.h"
#import "Texture.h"


@implementation VCPOpenGLController

#pragma mark -
#pragma mark Init and bookkeeping methods
+ (NSOpenGLPixelFormat *)defaultPixelFormat
{
    NSOpenGLPixelFormatAttribute attributes [] = {
        NSOpenGLPFAWindow,
		NSOpenGLPFADoubleBuffer,
		NSOpenGLPFADepthSize, 16,
	(NSOpenGLPixelFormatAttribute)nil };
	
    return [[[NSOpenGLPixelFormat alloc] initWithAttributes:attributes] autorelease];
}

- (void)initGL
{
	
	NSLog(@"In VCP view initGL routine");
	
	// Use the superclasses camera management
	[super initGL];
	
	// Set black background
	glClearColor(0.0, 0.0, 0.0, 1.0);
	
	// Set blending characteristics
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	// Generate texture ID's
	glGenTextures( 1, texIDs);
}

- (void)setRadar:(Radar *)newRadar
{
	radar = newRadar;
	
	
}

@end
